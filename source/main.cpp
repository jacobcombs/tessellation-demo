/**
 * @file
 *
 * This program clumsily demonstrates the application of tessellation shaders to the LOD problem. To see this, build
 * the program in its current state and move around the generated terrain.
 *
 * This program was originally authored by Jakob Progsch, and originally copyrighted as follows:
 *
 * Copyright (c) 2012 Jakob Progsch
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *    distribution.
 *
 * Original program documentation:
 *
 * OpenGL example code - Tesselation
 * 
 * This example shows the usage of tesselation for terrain LOD.
 * The terrain is given as a texture of 3d samples (generalized
 * heightfield) and gets rendered without use of a vbo/vao. Instead
 * sample coordinates are generated from InstanceID and VertexID.
 * Tessellation is used to dynamically change the amount of vertices
 * depending on distance from the viewer.
 * This example requires at least OpenGL 4.0
 * 
 * @authors {Jacob Combs, Jakob Progsch}
 */

#include <GL/glcorearb.h>
#include <GLXW/glxw.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/noise.hpp>
#include <glm/gtx/transform.hpp>

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <map>

/**
 * Generate terrain data.
 *
 * @param width width of terrain area
 * @param height height of terrain area
 *
 * @return vector of (width * height) positions on the terrain surface
 */
std::vector<glm::vec3> generate_terrain_data(int width, int height) {
  std::vector<glm::vec3> data(width * height);

  glm::vec3 layernorm = glm::normalize(glm::vec3(0.1f, 0.3f, 1.0f));
  glm::vec3 layerdir(0, 0, 1);
  layerdir -= layernorm * glm::dot(layernorm, layerdir);
  layerdir = glm::normalize(layerdir);

  for (int y = 0; y < height; ++y) {
    for (int x = 0; x < width; ++x) {
      glm::vec2 pos((float) x / width, (float) y / height);
      glm::vec3 tmp = glm::vec3(pos, 0.15f * glm::perlin(5.0f * pos));
      data[y * width + x] = tmp + 0.04f * layerdir * glm::perlin(glm::vec2(30.0f * glm::dot(layernorm, tmp), 0.5f));
    }
  }
  return data;
}

/**
 * Create and bind an OpenGL texture with the terrain displacement data.
 *
 * @param data terrain displacement data of dimension width-by-height
 * @param width width of terrain area
 * @param height height of terrain area
 *
 * @return texture identifier
 */
GLuint create_terrain_texture(std::vector<glm::vec3> data, int width, int height) {
  GLuint texture_handle;
  glGenTextures(1, &texture_handle);
  glBindTexture(GL_TEXTURE_2D, texture_handle);

  // Set texture parameters
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  // Set texture content
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, &data[0]);

  return texture_handle;
}

/**
 * Store and manipulate data associated with a GLFW window.
 */
struct WindowData {
  GLuint vao; // Vertex array object for drawing

  GLuint shader_program;
  std::map<std::string, GLint> uniform_locations;
  static const std::vector<std::string> uniform_names;

  GLuint displacement; // Terrain texture

  float time; // For timestep calculation
  glm::vec3 position; // Camera position
  glm::mat4 rotation; // Camera orientation

  bool tessellation = true; // Use tessellation flag
  bool space_down = true; // Space key is pressed flag

  double mousex; // Mouse position (x)
  double mousey; // Mouse position (y)

  GLFWwindow *window; // parent window

  /**
   * Create a WindowData struct to be associated with a GLFW window.
   *
   * @param win the GLFW window
   */
  WindowData(GLFWwindow *win) : shader_program(0), position(glm::vec3(0.0)), rotation(glm::mat4(1.0)),
                                tessellation(true), space_down(false) {
    window = win;
  }

  /**
   * Create and bind a vertex array object for drawing to this window.
   */
  void init_and_bind_vao() {
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
  }

  /**
   * Set the shader program associated with this window and initialize uniform location map.
   *
   * @param program shader program identifier
   */
  void set_shader_program(GLuint program) {
    shader_program = program;
    for (auto string : uniform_names) {
      uniform_locations[string] = glGetUniformLocation(shader_program, string.c_str());
    }
  }

  /**
   * Generate terrain data and initialize the associated OpenGL texture.
   */
  void setup_terrain_texture() {
    const int terrain_width = 1024;
    const int terrain_height = 1024;
    std::vector<glm::vec3> displacement_data = generate_terrain_data(terrain_width, terrain_height);
    displacement = create_terrain_texture(displacement_data, terrain_width, terrain_height);
  }

  /**
   * Initialize fields required by the main program loop: time and mouse coordinates.
   */
  void setup_for_rendering() {
    time = glfwGetTime();
    glfwGetCursorPos(window, &mousex, &mousey);
  }
};

const std::vector<std::string> WindowData::uniform_names = {
    "width",
    "height",
    "ViewProjection",
    "ViewPosition",
    "displacement",
    "tess_scale"
};

/**
 * Check for and display any shader compiler errors.
 *
 * @param obj shader identifier
 *
 * @return true if shader compile compiled successfully, false otherwise
 */
bool check_shader_compile_status(GLuint obj) {
  GLint status;
  glGetShaderiv(obj, GL_COMPILE_STATUS, &status);
  if (status == GL_FALSE) {
    GLint length;
    glGetShaderiv(obj, GL_INFO_LOG_LENGTH, &length);
    std::vector<char> log(length);
    glGetShaderInfoLog(obj, length, &length, &log[0]);
    std::cerr << &log[0];
    return false;
  }
  return true;
}

/**
 * Check for and display shader linker errors.
 *
 * @param obj program identifier
 *
 * @return true if program linked successfully, false otherwise
 */
bool check_program_link_status(GLuint obj) {
  GLint status;
  glGetProgramiv(obj, GL_LINK_STATUS, &status);
  if (status == GL_FALSE) {
    GLint length;
    glGetProgramiv(obj, GL_INFO_LOG_LENGTH, &length);
    std::vector<char> log(length);
    glGetProgramInfoLog(obj, length, &length, &log[0]);
    std::cerr << &log[0];
    return false;
  }
  return true;
}

/**
 * Create and compile a shader from a source file.
 *
 * @param type the type of shader (GL_*_SHADER)
 * @param source shader source file name
 *
 * @return shader handle (should be checked to ensure no errors occurred during compilation)
 */
GLuint create_and_compile_shader(GLenum type, std::string source_file) {
  GLuint shader = glCreateShader(type);

  std::ifstream shader_file(source_file);
  if (!shader_file) {
    std::cerr << "unable to locate shader file" << std::endl;
    return shader;
  }
  std::string source((std::istreambuf_iterator<char>(shader_file)),
                     std::istreambuf_iterator<char>()); // Read shader source into a string
  shader_file.close();
  const char *tmp_source = source.c_str();
  GLint tmp_length = source.size();
  glShaderSource(shader, 1, &tmp_source, &tmp_length);

  glCompileShader(shader);
  return shader;
}

/**
 * Initialize GLFW and create the window, exiting the program on failure.
 *
 * @param width window width
 * @param height window height
 * @param name window name
 *
 * @return pointer to the GLFW window
 */
GLFWwindow *create_glfw_window(int width = 1024, int height = 1024, const char *name = "tessellate") {
  if (glfwInit() == GL_FALSE) {
    std::cerr << "failed to init GLFW" << std::endl;
    std::exit(1);
  }

  // Select OpenGL version
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

  GLFWwindow *window = glfwCreateWindow(width, height, name, 0, 0);
  if (window == nullptr) {
    std::cerr << "failed to open window" << std::endl;
    glfwTerminate();
    std::exit(1);
  }

  glfwSetWindowUserPointer(window, new WindowData(window));

  glfwMakeContextCurrent(window);
  return window;
}

/**
 * Compile and link the shader program, exiting the program on failure.
 *
 * @param window the GLFW window, to be closed on failure before program exit
 *
 * @return true on success, false on failure
 */
bool setup_shader_program(GLFWwindow *window) {
  // Compile shaders
  GLuint vertex_shader = create_and_compile_shader(GL_VERTEX_SHADER, "source/shader.vert");
  if (!check_shader_compile_status(vertex_shader)) {
    return false;
  }
  GLuint tess_control_shader = create_and_compile_shader(GL_TESS_CONTROL_SHADER, "source/shader.tesc");
  if (!check_shader_compile_status(tess_control_shader)) {
    return false;
  }
  GLuint tess_eval_shader = create_and_compile_shader(GL_TESS_EVALUATION_SHADER, "source/shader.tese");
  if (!check_shader_compile_status(tess_eval_shader)) {
    return false;
  }
  GLuint fragment_shader = create_and_compile_shader(GL_FRAGMENT_SHADER, "source/shader.frag");
  if (!check_shader_compile_status(fragment_shader)) {
    return false;
  }

  // Link shaders
  GLuint shader_program = glCreateProgram();
  glAttachShader(shader_program, vertex_shader);
  glAttachShader(shader_program, tess_control_shader);
  glAttachShader(shader_program, tess_eval_shader);
  glAttachShader(shader_program, fragment_shader);
  glLinkProgram(shader_program);
  if (!check_program_link_status(shader_program)) {
    return false;
  }

  auto data = static_cast<WindowData *>(glfwGetWindowUserPointer(window));
  data->set_shader_program(shader_program);
  return true;
}

void update_window(GLFWwindow *window) {
  auto data = static_cast<WindowData *>(glfwGetWindowUserPointer(window));

  // Calculate timestep
  float new_t = glfwGetTime();
  float dt = new_t - data->time;
  data->time = new_t;

  // Update mouse differential
  double tmpx, tmpy;
  glfwGetCursorPos(window, &tmpx, &tmpy);
  glm::vec2 mousediff(tmpx - data->mousex, tmpy - data->mousey);
  data->mousex = tmpx;
  data->mousey = tmpy;

  // Find up, forward and right vector
  glm::mat3 rotation3(data->rotation);
  glm::vec3 up = glm::transpose(rotation3) * glm::vec3(0.0f, 1.0f, 0.0f);
  glm::vec3 right = glm::transpose(rotation3) * glm::vec3(1.0f, 0.0f, 0.0f);
  glm::vec3 forward = glm::transpose(rotation3) * glm::vec3(0.0f, 0.0f, -1.0f);

  // Apply mouse rotation
  data->rotation = glm::rotate(data->rotation, 0.2f * mousediff.x, up);
  data->rotation = glm::rotate(data->rotation, 0.2f * mousediff.y, right);

  // Roll
  if (glfwGetKey(window, 'Q')) {
    data->rotation = glm::rotate(data->rotation, 180.0f * dt, forward);
  }
  if (glfwGetKey(window, 'E')) {
    data->rotation = glm::rotate(data->rotation, -180.0f * dt, forward);
  }

  // Movement
  float speed = 0.1f;
  if (glfwGetKey(window, 'W')) {
    data->position += speed * dt * forward;
  }
  if (glfwGetKey(window, 'S')) {
    data->position -= speed * dt * forward;
  }
  if (glfwGetKey(window, 'D')) {
    data->position += speed * dt * right;
  }
  if (glfwGetKey(window, 'A')) {
    data->position -= speed * dt * right;
  }

  // Wireframe
  if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT)) {
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  } else {
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  }

  // Toggle tesselation
  if (glfwGetKey(window, GLFW_KEY_SPACE) && !data->space_down) {
    data->tessellation = !data->tessellation;
  }
  data->space_down = glfwGetKey(window, GLFW_KEY_SPACE);

  // Calculate ViewProjection matrix
  int width, height;
  glfwGetWindowSize(window, &width, &height);
  glm::mat4 Projection = glm::perspective(60.0f, (float) width / height, 0.001f, 10.f);
  glm::mat4 View = data->rotation * glm::translate(glm::mat4(1.0f), -data->position);
  glm::mat4 ViewProjection = Projection * View;

  // Clear
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, data->displacement);

  // Use the shader program
  glUseProgram(data->shader_program);
  glUniform1ui(data->uniform_locations["width"], 64); // 64x64 base grid without tessellation
  glUniform1ui(data->uniform_locations["height"], 64);
  glUniformMatrix4fv(data->uniform_locations["ViewProjection"], 1, GL_FALSE, glm::value_ptr(ViewProjection));
  glUniform3fv(data->uniform_locations["ViewPosition"], 1, glm::value_ptr(data->position));

  if (data->tessellation) {
    glUniform1f(data->uniform_locations["tess_scale"], 1.0f);
  } else {
    glUniform1f(data->uniform_locations["tess_scale"], 0.0f);
  }

  // Set texture uniform
  glUniform1i(data->uniform_locations["displacement"], 0);

  // Draw terrain
  glDrawArraysInstanced(GL_PATCHES, 0, 6, 64 * 64);

  // Finally, swap buffers
  glfwSwapBuffers(window);
}

int main() {
  GLFWwindow *window = create_glfw_window();
  auto data = static_cast<WindowData *>(glfwGetWindowUserPointer(window));

  if (glxwInit()) {
    std::cerr << "failed to init GL3W" << std::endl;
    glfwDestroyWindow(window);
    glfwTerminate();
    std::exit(1);
  }

  data->init_and_bind_vao(); // Set up VAO for drawing

  if (!setup_shader_program(window)) {
    glfwDestroyWindow(window);
    glfwTerminate();
    std::exit(1);
  }

  data->setup_terrain_texture(); // Create and bind terrain texture

  // todo: move these miscellaneous tasks to the appropriate init methods
  glEnable(GL_DEPTH_TEST);
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  data->setup_for_rendering();

  while (!glfwWindowShouldClose(window)) {
    glfwPollEvents();
    update_window(window);

    // Check for errors
    GLenum error = glGetError();
    if (error != GL_NO_ERROR) {
      std::cerr << error << std::endl;
      break;
    }
  }

  glDeleteVertexArrays(1, &data->vao);
  glDeleteProgram(data->shader_program);
  glfwDestroyWindow(window);
  glfwTerminate();
  return 0;
}