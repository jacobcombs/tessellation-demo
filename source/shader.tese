# version 400

uniform mat4 ViewProjection;
uniform sampler2D displacement;

layout(triangles, equal_spacing, cw) in;
in vec4 tcposition[];

out vec2 tecoord;
out vec4 teposition;

void main()
{
    teposition = gl_TessCoord.x * tcposition[0];
    teposition += gl_TessCoord.y * tcposition[1];
    teposition += gl_TessCoord.z * tcposition[2];
    tecoord = teposition.xy;

    vec3 offset = texture(displacement, tecoord).xyz;
    teposition.xyz = offset;
    gl_Position = ViewProjection*teposition;
}
