# version 400

uniform vec3 ViewPosition;
uniform sampler2D displacement;

in vec4 teposition;
in vec2 tecoord;

layout(location = 0) out vec4 FragColor;

void main()
{
    vec3 x = textureOffset(displacement, tecoord, ivec2(0, 0)).xyz;
    vec3 t0 = x - textureOffset(displacement, tecoord, ivec2(1, 0)).xyz;
    vec3 t1 = x - textureOffset(displacement, tecoord, ivec2(0, 1)).xyz;

    vec3 normal = (gl_FrontFacing ? 1 : -1) * normalize(cross(t0, t1));
    vec3 light = normalize(vec3(2, -1, 3));
    vec3 reflected = reflect(normalize(ViewPosition - teposition.xyz), normal);

    float ambient = 0.3;
    float diffuse = max(0, dot(normal, light));
    float specular = pow(max(0, dot(reflected, light)), 64);

    FragColor = vec4(vec3(ambient + 0.5 * diffuse + 0.4 * specular), 1);
}
