# version 400

uniform uint width;
uniform uint height;

out vec4 tposition;

const vec2 quad_offsets[6] = vec2[](
    vec2(0, 0), vec2(1, 0), vec2(1, 1),
    vec2(0, 0), vec2(1, 1), vec2(0, 1)
);

void main()
{
    vec2 base = vec2(gl_InstanceID / width, gl_InstanceID % width);
    vec2 offset = quad_offsets[gl_VertexID];
    vec2 pos = (base + offset) / vec2(width + 1, height + 1);
    tposition = vec4(pos, 0, 1);
}
