# version 400

uniform vec3 ViewPosition;
uniform float tess_scale;

layout(vertices = 3) out;

in vec4 tposition[];

out vec4 tcposition[];

void main()
{
    tcposition[gl_InvocationID] = tposition[gl_InvocationID];

    if(gl_InvocationID == 0) {
        vec3 terrainpos = ViewPosition;
        terrainpos.z -= clamp(terrainpos.z, -0.1, 0.1);

        vec4 center = (tposition[1] + tposition[2]) / 2.0;
        //gl_TessLevelOuter[0] = min(6.0, 1 + tess_scale * 0.5 / distance(center.xyz, terrainpos));
        gl_TessLevelOuter[0] = 1 + tess_scale * 0.5 / pow(distance(center.xyz, terrainpos), 1);

        center = (tposition[2] + tposition[0]) / 2.0;
        //gl_TessLevelOuter[1] = min(6.0, 1 + tess_scale * 0.5 / distance(center.xyz, terrainpos));
        gl_TessLevelOuter[1] = 1 + tess_scale * 0.5 / pow(distance(center.xyz, terrainpos), 1);

        center = (tposition[0] + tposition[1]) / 2.0;
        //gl_TessLevelOuter[2] = min(6.0, 1 + tess_scale * 0.5 / distance(center.xyz, terrainpos));
        gl_TessLevelOuter[2] = 1 + tess_scale * 0.5 / pow(distance(center.xyz, terrainpos), 1);

        center = (tposition[0] + tposition[1] + tposition[2]) / 3.0;
        //gl_TessLevelInner[0] = min(7.0, 1 + tess_scale * 0.7 / distance(center.xyz, terrainpos));
        gl_TessLevelInner[0] = 1 + tess_scale * 0.7 / pow(distance(center.xyz, terrainpos), 1);
    }
}
