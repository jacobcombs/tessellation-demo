cmake_minimum_required(VERSION 3.7)
project(tessellation_demo)

set(CMAKE_CXX_STANDARD 11)

add_subdirectory(glfw)
add_subdirectory(glxw)

include_directories(glfw/include)
include_directories(glm)
include_directories(${CMAKE_BINARY_DIR}/glxw/include)

set(EXTERNAL_LIBS
    ${OPENGL_LIBRARY} glfw ${GLFW_LIBRARIES} glxw)

set(SOURCE_FILES
    source/main.cpp)

set(EXECUTABLE tessellate)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR})
add_executable(${EXECUTABLE} ${SOURCE_FILES})
target_link_libraries(${EXECUTABLE} ${EXTERNAL_LIBS})