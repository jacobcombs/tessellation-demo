# Tessellation Demo

This repo contains a demonstration of OpenGL tessellation shaders applied to the Level-of-Detail (LOD) problem. It aims to satisfy the requirements of the graduate student project in the Spring 2017 Computer Graphics course at University of Arizona under instructors Phil Amburn and Jixian Li (T.A.).

## Build and run

The code is intended to run on the Macs we have been using for the course, and was developed on my personal laptop (MacOS). Many external libraries are included with this project, namely GLM, GLFW, and GLXW. See `Makefile` for details on how to build the project.

To get started right away, invoke `make`, and run the executable `tessellate`, generated in the top level directory. Note that `make` does not complete successfully (there is an error about a missing `'EGL/egl.h'`), but the executable is still created.

## Code overview

The source code resides in the `source/` directory. See `source/main.cpp` for a short description of the program and some documentation from the original codebase. The other files in that directory contain shader code.

## Presentation slides

The `doc` directory contains presentation slides (in several formats) that explain concepts foundational to understanding OpenGL tessellation shaders.
